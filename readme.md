# Admin Auth

## Резюме

Название: Admin Auth  
Домен: Units  
Назначение: Авторизация административных пользователей  

## Разработка сервиса

Инструкцию описывающую разворот, запуск и тестирование сервиса на локальной машине можно найти в отдельном документе в [Confluence](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/362676232/Backend-)

Регламент работы над задачами тоже находится в [Confluence](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/477528081)

### Настройка авторизации

Для авторизации используется Laravel Passport, при первоначальном развороте требуется его настройка.

Необходимо:
1. Сгенерировать ключи
   
Генерируются командой:

`php artisan passport:keys`

С помощью env переменной PASSPORT_KEYS_PATH можно задать путь для размещения ключей.

2. Создать клиентов
   
Клиенты создаются для каждого *-gui-backend. Для текущих целей при авторизации используется Password Grant Type. Соответственно для создания клиента с данным типом авторизации используется команда:

`php artisan passport:client --password`

При выполнении команды желательно задать кастомное имя клиента, но можно оставить и дефолтное. Провайдер оставляем дефолтный.

После создания клиента в консоль выводятся *Client ID* и *Client secret*. Их необходимо добавить в енв файл соответствующего *-gui-backend сервиса в виде переменных *UNITS_ADMIN_AUTH_SERVICE_CLIENT_ID* и *UNITS_ADMIN_AUTH_SERVICE_CLIENT_SECRET*.

## Структура сервиса

Почитать про структуру сервиса можно здесь [здесь](docs/structure.md)

## Зависимости

| Название | Описание  | Переменные окружения |
|---|---|---|
| PostgreSQL | Основная БД сервиса | DB_CONNECTION<br/>DB_HOST<br/>DB_PORT<br/>DB_DATABASE<br/>DB_USERNAME<br/>DB_PASSWORD |
| Kafka | Брокер сообщений<br/>Producer осуществляет запись в следующие топики:<br/> - `<контур>.admin-auth.fact.generated-password-token.1`<br/> - `<контур>.admin-auth.fact.updated-user.1`<br/> - `<контур>.admin-auth.fact.deactivated-user.1`<br/> Consumer (слушатель) в данном сервисе не используется | KAFKA_CONTOUR<br/>KAFKA_BROKER_LIST<br/>KAFKA_SECURITY_PROTOCOL<br/>KAFKA_SASL_MECHANISMS<br/>KAFKA_SASL_USERNAME<br/>KAFKA_SASL_PASSWORD<br/> |

## Среды

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/units/job/admin-auth/  
URL: https://user-auth-master-dev.ensi.tech/docs/swagger  

### Preprod

Отсутствует

### Prod

Отсутствует

## Контакты

Команда поддерживающая данный сервис: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email для связи: mail@greensight.ru

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
