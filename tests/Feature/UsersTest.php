<?php

namespace Tests\Feature;

use App\Domain\Users\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\TestCase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Indicates whether the default seeder should run before each test.
     *
     * @var bool
     */
    protected $seed = true;

    private $expectedUserResponseFields =  [
        'id', 'email', 'login', 'created_at', 'updated_at', 'last_name', 'first_name',  'middle_name', 'email', 'phone', 'active', 'full_name', 'short_name', 'timezone',
    ];

    public function testSearchUsers()
    {
        /** @var TestResponse $response */
        $response = $this->post('/api/v1/users:search');
        $response->assertStatus(200);
        $this->assertCount(10, $response->json('data'));
        $response->assertJsonStructure([
            'data' => [
                0 => $this->expectedUserResponseFields,
            ],
        ]);
    }

    public function testGetUser()
    {
        /** @var TestResponse $response */
        $response = $this->get('/api/v1/users/1');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => $this->expectedUserResponseFields,
        ]);
    }

    public function testCreateUser()
    {
        /** @var TestResponse $response */
        $response = $this->post('/api/v1/users', [
            'login' => 'test',
            'email' => 'test@email.ru',
            'password' => '123456',
            'phone' => '+79191000000',
            'timezone' => 'Europe/Moscow',
        ]);
        $response->assertJsonStructure([
            'data' => $this->expectedUserResponseFields,
        ]);
    }

    public function testPatchUser()
    {
        /** @var TestResponse $response */
        $response = $this->patch('/api/v1/users/10', [
            'email' => 'test@email.ua',
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => $this->expectedUserResponseFields,
        ]);

        $countUsers = User::query()->where('id', 10)->where('email', 'test@email.ua')->count();
        $this->assertEquals(1, $countUsers);
    }

    public function testDeleteUser()
    {
        /** @var TestResponse $response */
        $response = $this->delete('/api/v1/users/10');
        $response->assertStatus(200);
        $countUsers = User::query()->where('id', 10)->count();
        $this->assertEquals(0, $countUsers);
    }
}
