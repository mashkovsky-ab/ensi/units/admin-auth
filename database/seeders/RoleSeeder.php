<?php

namespace Database\Seeders;

use App\Domain\Users\Models\Role;
use App\Http\ApiV1\OpenApiGenerated\Enums\RoleEnum;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    public static array $roles = [
        [
            'id' => RoleEnum::ADMIN,
            'title' => 'admin',
        ],
        [
            'id' => RoleEnum::SUPER_ADMIN,
            'title' => 'super_admin',
        ],
        [
            'id' => RoleEnum::SELLER_ADMINISTRATOR,
            'title' => 'seller_administrator',
        ],
        [
            'id' => RoleEnum::SEO_MANAGER,
            'title' => 'seo_manager',
        ],
        [
            'id' => RoleEnum::OMS_SHIFT_MANAGER,
            'title' => 'oms_shift_manager',
        ],
        [
            'id' => RoleEnum::OMS_ORDER_COLLECTOR,
            'title' => 'oms_order_collector',
        ],
        [
            'id' => RoleEnum::OMS_ORDER_PICKER,
            'title' => 'oms_order_picker',
        ],
        [
            'id' => RoleEnum::OMS_CALL_CENTER,
            'title' => 'oms_call_center',
        ],
        [
            'id' => RoleEnum::SAS_SELLER_OPERATOR,
            'title' => 'sas_seller_operator',
        ],
        [
            'id' => RoleEnum::SAS_SELLER_ADMIN,
            'title' => 'sas_seller_admin',
        ],
        [
            'id' => RoleEnum::OMS_STORE_EMPLOYEE,
            'title' => 'oms_store_employee',
        ],
        [
            'id' => RoleEnum::OMS_LOGISTIC,
            'title' => 'oms_logistic',
        ],
        [
            'id' => RoleEnum::PIM_MANAGER,
            'title' => 'pim_manager',
        ],
        [
            'id' => RoleEnum::MANAGER_SELLER,
            'title' => 'manager_seller',
        ],
        [
            'id' => RoleEnum::MANAGER_CLIENT,
            'title' => 'manager_client',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::$roles as $role) {
            $roleModel = Role::query()->where('id', $role['id'])->first();

            if ($roleModel === null) {
                $roleModel = new Role();
                $roleModel->id = $role['id'];
            }

            $roleModel->title = $role['title'];
            $roleModel->save();
        }
    }
}
