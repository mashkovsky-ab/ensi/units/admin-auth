<?php

namespace App\Domain\Kafka;

class TopicNameBuilder
{
    public static function fact(string $description, int $version = 1): string
    {
        return static::build('fact', $description, $version);
    }

    public static function command(string $description, int $version = 1): string
    {
        return static::build('command', $description, $version);
    }

    public static function cdc(string $description, int $version = 1): string
    {
        return static::build('cdc', $description, $version);
    }

    public static function test(string $description, int $version = 1): string
    {
        return static::build('test', $description, $version);
    }

    public static function internal(string $description, int $version = 1): string
    {
        return static::build('internal', $description, $version);
    }

    private static function build(string $type, string $description, int $version = 1): string
    {
        $contour = config('app.env');

        return "$contour.admin-auth.$type.$description.$version";
    }
}
