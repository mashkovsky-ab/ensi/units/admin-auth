<?php

namespace App\Domain\Kafka\Actions\Send;

use Ensi\LaravelPhpRdKafkaProducer\HighLevelProducer;

class SendMessageAction
{
    public function execute(string $topicName, string $message)
    {
        (new HighLevelProducer($topicName))->sendOne($message);
    }
}
