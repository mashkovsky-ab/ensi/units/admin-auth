<?php

namespace App\Domain\Users\Observers;

use App\Domain\Kafka\Actions\Send\SendMessageAction;
use App\Domain\Kafka\TopicNameBuilder;
use App\Domain\Users\Models\User;

class UserObserver
{
    public function __construct(
    ) {
    }

    public function creating(User $user)
    {
        $user->generatePasswordToken();
    }

    public function created(User $user)
    {
        $sendMessageAction = resolve(SendMessageAction::class);
        $message = json_encode([
            'user_id' => $user->id,
            'token' => $user->password_token,
        ]);
        $sendMessageAction->execute(TopicNameBuilder::fact('generated-password-token'), $message);
    }

    public function updating(User $user)
    {
        if ($user->isDirty('password')) {
            $user->destroyPasswordToken();
        }
    }

    public function updated(User $user)
    {
        if ($user->isActive() && !$user->isDirty('password_token')) {
            $sendMessageAction = resolve(SendMessageAction::class);
            $message = json_encode([
                'user_id' => $user->id,
            ]);
            $sendMessageAction->execute(TopicNameBuilder::fact('updated-user'), $message);
        }
    }
}
