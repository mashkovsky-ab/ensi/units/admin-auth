<?php

namespace App\Domain\Users\Tests\Factories;

use App\Domain\Users\Models\Role;
use App\Domain\Users\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'login' => $this->faker->unique()->userName(),
            'password' => $this->faker->password(),
            'last_name' => $this->faker->lastName(),
            'active' => $this->faker->boolean(),
            'first_name' => $this->faker->firstName(),
            'middle_name' => $this->faker->firstName(),
            'phone' => $this->faker->unique()->numerify('+7##########'),
            'email' => $this->faker->unique()->safeEmail(),
            'timezone' => $this->faker->timezone(),
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function (User $user) {
            $roles = Role::all();

            $user->roles()->attach(
                $roles->random(1)->pluck('id')->toArray()
            );
        });
    }

    public function notActive()
    {
        return $this->state(function (array $attributes) {
            return [
                'active' => false,
            ];
        });
    }
}
