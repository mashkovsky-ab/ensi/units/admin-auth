<?php

namespace App\Domain\Users\Actions;

use App\Domain\Kafka\Actions\Send\SendMessageAction;
use App\Domain\Kafka\TopicNameBuilder;
use App\Domain\Users\Models\User;

class PatchUserAction
{
    public function execute(int $userId, array $fields): User
    {
        $user = User::findOrFail($userId);
        $user->update($fields);

        if (isset($fields['active']) && $fields['active'] == false) {
            $sendMessageAction = resolve(SendMessageAction::class);

            $message = json_encode([
                'user_id' => $user->id,
                'cause_deactivation' => $fields['cause_deactivation'],
            ]);
            $sendMessageAction->execute(TopicNameBuilder::fact('deactivated-user'), $message);
        }

        return $user;
    }
}
