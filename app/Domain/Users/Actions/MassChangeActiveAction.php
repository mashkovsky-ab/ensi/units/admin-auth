<?php

namespace App\Domain\Users\Actions;

use App\Domain\Kafka\Actions\Send\SendMessageAction;
use App\Domain\Kafka\TopicNameBuilder;
use App\Domain\Users\Models\User;

class MassChangeActiveAction
{
    public function execute(array $data): void
    {
        $userIds = $data['user_ids'];
        $active = $data['active'];

        User::query()->whereIn('id', $userIds)->update([
            'active' => $active,
        ]);
        if (!$active) {
            $sendMessageAction = resolve(SendMessageAction::class);
            $topicName = TopicNameBuilder::fact('deactivated-user');
            foreach ($userIds as $userId) {
                $message = json_encode([
                    'user_id' => $userId,
                    'cause_deactivation' => $data['cause_deactivation'],
                ]);
                $sendMessageAction->execute($topicName, $message);
            }
        }
    }
}
