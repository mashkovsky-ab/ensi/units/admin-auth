<?php

namespace App\Domain\Users\Actions;

use App\Domain\Kafka\Actions\Send\SendMessageAction;
use App\Domain\Kafka\TopicNameBuilder;
use App\Domain\Users\Models\User;

class RefreshPasswordTokenAction
{
    public function execute(int $userId): void
    {
        /** @var User $user */
        $user = User::findOrFail($userId);
        $user->generatePasswordToken();
        $user->save();

        $sendMessageAction = resolve(SendMessageAction::class);
        $message = json_encode([
            'user_id' => $user->id,
            'token' => $user->password_token,
        ]);
        $sendMessageAction->execute(TopicNameBuilder::fact('generated-password-token'), $message);
    }
}
