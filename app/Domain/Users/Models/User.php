<?php

namespace App\Domain\Users\Models;

use App\Domain\Users\Tests\Factories\UserFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

/**
 * @property array $attributes
 *
 * @property int $id
 * @property string $login
 * @property string|null $password
 * @property bool $active
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $phone
 * @property string $email
 * @property string $timezone
 * @property string $password_token
 * @property Carbon $password_token_created_at
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read string $full_name
 * @property-read string $short_name
 * @property-read Collection|Role[] $roles
 */
class User extends Authenticatable
{
    use HasApiTokens;
    use Notifiable;
    use HasFactory;

    const LIFE_TIME_PASSWORD_TOKEN = 3;

    /**
     * Заполняемые поля модели
     */
    const FILLABLE = ['login', 'password', 'active', 'first_name', 'last_name', 'middle_name', 'email', 'phone', 'timezone'];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    /**
     * @var array
     */
    protected $hidden = ['password'];

    /**
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class)->using(UserRole::class)->withPivot('expires')->withTimestamps();
    }

    /**
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = $value ? User::encryptPassword($value) : null;
    }

    /**
     * @param $password
     * @return string
     */
    public static function encryptPassword($password)
    {
        return Hash::make($password);
    }

    /**
     * @param $password
     * @return bool
     */
    public function checkPassword($password)
    {
        return Hash::check($password, $this->password);
    }

    public function generatePasswordToken(): void
    {
        $this->password_token = md5($this->login . rand(PHP_INT_MIN, PHP_INT_MAX));
        $this->password_token_created_at = now();
    }

    public function destroyPasswordToken(): void
    {
        $this->password_token = null;
        $this->password_token_created_at = null;
    }

    public function isActive(): bool
    {
        return $this->active == true;
    }

    /**
     * Override the field which is used for username in the Laravel Passport authentication
     *
     * @param string $login
     */
    public function findForPassport(string $login)
    {
        return $this->where('login', $login)->first();
    }

    /**
     * Add a password validation callback for Laravel Passport
     *
     * @param type $password
     * @return bool Whether the password is valid
     */
    public function validateForPassportPasswordGrant($password)
    {
        return $this->checkPassword($password);
    }

    public function getFullNameAttribute(): string
    {
        $pieces = [];
        if ($this->last_name) {
            $pieces[] = $this->last_name;
        }
        if ($this->first_name) {
            $pieces[] = $this->first_name;
        }
        if ($this->middle_name) {
            $pieces[] = $this->middle_name;
        }

        return implode(' ', $pieces);
    }

    public function getShortNameAttribute(): string
    {
        $pieces = [];
        if ($this->last_name) {
            $pieces[] = $this->last_name;
        }
        if ($this->first_name) {
            $pieces[] = mb_substr($this->first_name, 0, 1) . '.';
        }
        if ($this->middle_name) {
            $pieces[] = mb_substr($this->middle_name, 0, 1) . '.';
        }

        return implode(' ', $pieces);
    }

    public static function factory(): UserFactory
    {
        return UserFactory::new();
    }
}
