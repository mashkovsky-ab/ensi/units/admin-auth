<?php

namespace App\Domain\Users\Models;

use App\Domain\Users\Tests\Factories\RoleFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property Carbon|null $expires
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read User $user
 */
class Role extends Model
{
    use HasFactory;

    public $incrementing = false;

    public static function factory(): RoleFactory
    {
        return RoleFactory::new();
    }
}
