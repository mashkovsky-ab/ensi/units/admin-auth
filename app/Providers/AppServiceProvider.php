<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Model;
use App\Domain\Users\Models\User;
use App\Domain\Users\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Model::preventLazyLoading(!app()->isProduction());
        User::observe(UserObserver::class);
    }
}
