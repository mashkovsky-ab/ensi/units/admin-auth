<?php

use App\Domain\Users\Models\Role;
use App\Http\ApiV1\OpenApiGenerated\Enums\RoleEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Database\Seeders\RoleSeeder;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\seed;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/roles/{id} 200', function () {
    seed(RoleSeeder::class);
    $roleId = RoleEnum::ADMIN;

    getJson("/api/v1/roles/$roleId")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $roleId);
});

test('GET /api/v1/roles/{id} 404', function () {
    getJson("/api/v1/roles/1000")
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test("POST /api/v1/roles:search 200", function () {
    $roles = Role::factory()->count(3)->create();
    $lastId = $roles->sortBy('id')->last()->id;

    $requestBody = [
        "sort" => [
            "-id",
        ],
    ];

    postJson("/api/v1/roles:search", $requestBody)
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $lastId);
});
