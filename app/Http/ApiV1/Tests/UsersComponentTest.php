<?php

use App\Domain\Kafka\Actions\Send\SendMessageAction;
use App\Domain\Users\Models\Role;
use App\Domain\Users\Models\User;
use App\Domain\Users\Models\UserRole;
use App\Http\ApiV1\OpenApiGenerated\Enums\GrantTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\RoleEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use App\Http\ApiV1\Tests\Factories\UserRequestFactory;
use Database\Seeders\RoleSeeder;
use Laravel\Passport\Client;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\seed;
use function PHPUnit\Framework\assertNotEquals;
use Tests\IntegrationTestCase;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/users 201', function () {
    /** @var IntegrationTestCase $this */
    $this->mock(SendMessageAction::class)->shouldReceive('execute');
    $userData = UserRequestFactory::new()->make();

    postJson('/api/v1/users', $userData)
        ->assertStatus(201)
        ->assertJsonPath('data.login', $userData['login']);

    assertDatabaseHas('users', [
        'login' => $userData['login'],
        'active' => $userData['active'],
    ]);
});

test('POST /api/v1/users 400', function () {
    $userData = UserRequestFactory::new()->make();
    $userData['phone'] = '1234';

    postJson('/api/v1/users', $userData)
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', "ValidationError");
});

test('GET /api/v1/users/{id} 200', function () {
    seed(RoleSeeder::class);
    /** @var IntegrationTestCase $this */
    $this->mock(SendMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create();
    $id = $user->id;

    getJson("/api/v1/users/$id")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id);
});

test('GET /api/v1/users/{id} 404', function () {
    getJson("/api/v1/users/1000")
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('PATCH /api/v1/users/{id} 200', function () {
    seed(RoleSeeder::class);
    /** @var IntegrationTestCase $this */
    $this->mock(SendMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create(['phone' => '+79245643567', 'first_name' => 'Ivan']);
    $id = $user->id;
    $userData = UserRequestFactory::new()->only(['phone'])->make(['phone' => '+79245643777']);

    patchJson("/api/v1/users/$id", $userData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id)
        ->assertJsonPath('data.phone', $userData['phone'])
        ->assertJsonPath('data.first_name', "Ivan");

    assertDatabaseHas('users', [
        'id' => $id,
        'phone' => $userData['phone'],
        'first_name' => "Ivan",
    ]);
});

test('PATCH /api/v1/users/{id} 404', function () {
    patchJson('/api/v1/users/1000', UserRequestFactory::new()->make())
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('DELETE /api/v1/users/{id} success', function () {
    seed(RoleSeeder::class);
    /** @var IntegrationTestCase $this */
    $this->mock(SendMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create(['phone' => '+79245643567', 'first_name' => 'Ivan']);
    $id = $user->id;

    deleteJson("/api/v1/users/$id")->assertStatus(200);

    assertModelMissing($user);
});

test("POST /api/v1/users:search 200", function () {
    seed(RoleSeeder::class);
    /** @var IntegrationTestCase $this */
    $this->mock(SendMessageAction::class)->shouldReceive('execute');
    $users = User::factory()
        ->count(10)
        ->sequence(
            ['active' => true],
            ['active' => false],
        )
        ->create();
    $lastId = $users->last()->id;

    $requestBody = [
        "filter" => [
            "active" => false,
        ],
        "sort" => [
            "-id",
        ],
    ];

    postJson("/api/v1/users:search", $requestBody)
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $lastId)
        ->assertJsonPath('data.0.active', false);
});


test("POST /api/v1/users:search-one 200", function () {
    seed(RoleSeeder::class);
    /** @var IntegrationTestCase $this */
    $this->mock(SendMessageAction::class)->shouldReceive('execute');
    $users = User::factory()
        ->count(10)
        ->create();
    /** @var User $user */
    $user = $users->random();
    $userId = $user->id;
    $userLogin = $user->login;

    $requestBody = [
        "filter" => [
            "login" => $userLogin,
        ],
    ];

    postJson("/api/v1/users:search-one", $requestBody)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $userId)
        ->assertJsonPath('data.login', $userLogin);
});

//test("POST /api/v1/users:current 200", function () {
//    seed(RoleSeeder::class);
//    /** @var IntegrationTestCase $this */
//    $this->mock(SendMessageAction::class)->shouldReceive('execute');
//
//    $client = Client::factory()->asPasswordClient()->create();
//
//    $password = 'Test1234!';
//    /** @var User $user */
//    $user = User::factory()->create(['password' => $password]);
//
//    $requestBody = [
//        "grant_type" => GrantTypeEnum::PASSWORD,
//        "client_id" => $client['id'],
//        "client_secret" => $client['secret'],
//        "scope" => [],
//        "username" => $user->login,
//        "password" => $password,
//    ];
//
//    $testResponse = postJson('/api/v1/oauth/token', $requestBody);
//
//    $token = json_decode($testResponse->baseResponse->content())->access_token;
//
//    postJson('/api/v1/users:current', [], ['Authorization' => "Bearer $token"])
//        ->assertStatus(200)
//        ->assertJsonPath('data.id', $user->id);
//});

test("POST /api/v1/users/{id}:add-roles 200", function () {
    seed(RoleSeeder::class);
    /** @var IntegrationTestCase $this */
    $this->mock(SendMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create();
    $id = $user->id;
    $requestBody = [
        'roles' => [
            RoleEnum::ADMIN,
            RoleEnum::PIM_MANAGER,
        ],
    ];

    postJson("/api/v1/users/{$id}:add-roles", $requestBody)
        ->assertStatus(200);

    assertDatabaseHas('role_user', [
        'user_id' => $id,
        'role_id' => RoleEnum::ADMIN,
    ]);

    assertDatabaseHas('role_user', [
        'user_id' => $id,
        'role_id' => RoleEnum::PIM_MANAGER,
    ]);
});

test("POST /api/v1/users/{id}:add-roles 400", function () {
    seed(RoleSeeder::class);
    /** @var IntegrationTestCase $this */
    $this->mock(SendMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create();
    $id = $user->id;
    $requestBody = [
        'roles' => [0],
    ];

    postJson("/api/v1/users/{$id}:add-roles", $requestBody)
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', "ValidationError");
});

test("POST /api/v1/users/{id}:delete-role 200", function () {
    seed(RoleSeeder::class);
    /** @var IntegrationTestCase $this */
    $this->mock(SendMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create();
    $id = $user->id;
    /** @var Role $role */
    $role = $user->roles->first();

    $requestBody = [
        'role_id' => $role->id,
    ];
    $userRole = UserRole::query()
        ->where('user_id', $id)
        ->where('role_id', $role->id)
        ->get()->first();

    postJson("/api/v1/users/{$id}:delete-role", $requestBody)
        ->assertStatus(200);

    assertModelMissing($userRole);
});

test("POST /api/v1/users/{id}:delete-role 400", function () {
    seed(RoleSeeder::class);
    /** @var IntegrationTestCase $this */
    $this->mock(SendMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create();
    $id = $user->id;

    $requestBody = [
        'role_id' => 0,
    ];

    postJson("/api/v1/users/{$id}:delete-role", $requestBody)
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', "ValidationError");
});

test("POST /api/v1/users/{id}:refresh-password-token 200", function () {
    seed(RoleSeeder::class);
    /** @var IntegrationTestCase $this */
    $this->mock(SendMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $user = User::factory()->create();
    $id = $user->id;

    postJson("/api/v1/users/{$id}:refresh-password-token")
        ->assertStatus(200);

    /** @var User $updatedUser */
    $updatedUser = User::findOrFail($id);

    assertNotEquals($user->password_token, $updatedUser->password_token);
});

test("POST /api/v1/users/mass/change-active activate users 200", function () {
    seed(RoleSeeder::class);
    /** @var IntegrationTestCase $this */
    $this->mock(SendMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $users = User::factory()->count(5)->create(['active' => false]);

    $requestBody = [
        'user_ids' => $users->pluck('id'),
        'active' => true,
    ];
    postJson("/api/v1/users/mass/change-active", $requestBody)
        ->assertStatus(200);

    assertDatabaseMissing('users', [
        'active' => false,
    ]);
});

test("POST /api/v1/users/mass/change-active deactivate users 200", function () {
    seed(RoleSeeder::class);
    /** @var IntegrationTestCase $this */
    $this->mock(SendMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $users = User::factory()->count(5)->create(['active' => true]);

    $requestBody = [
        'user_ids' => $users->pluck('id'),
        'active' => false,
        'cause_deactivation' => 'Some cause',
    ];
    postJson("/api/v1/users/mass/change-active", $requestBody)
        ->assertStatus(200);

    assertDatabaseMissing('users', [
        'active' => true,
    ]);
});

test("POST /api/v1/users/mass/change-active 400", function () {
    seed(RoleSeeder::class);
    /** @var IntegrationTestCase $this */
    $this->mock(SendMessageAction::class)->shouldReceive('execute');
    /** @var User $user */
    $users = User::factory()->count(5)->create(['active' => true]);

    $requestBody = [
        'user_ids' => $users->pluck('id'),
        'active' => false,
    ];
    postJson("/api/v1/users/mass/change-active", $requestBody)
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', "ValidationError");
});
