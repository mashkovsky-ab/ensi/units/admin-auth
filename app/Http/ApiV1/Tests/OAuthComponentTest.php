<?php
//
//use App\Domain\Kafka\Actions\Send\SendMessageAction;
//use App\Domain\Users\Models\User;
//use App\Http\ApiV1\OpenApiGenerated\Enums\GrantTypeEnum;
//use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
//use Database\Seeders\RoleSeeder;
//use Laravel\Passport\Client;
//use Lcobucci\JWT\Encoding\JoseEncoder;
//use Lcobucci\JWT\Token\Parser;
//
//use function Pest\Laravel\assertDatabaseHas;
//use function Pest\Laravel\deleteJson;
//use function Pest\Laravel\postJson;
//use function Pest\Laravel\seed;
//use Tests\IntegrationTestCase;
//
//uses(ApiV1ComponentTestCase::class);
//uses()->group('component');
//
//test('POST /api/v1/oauth/token success', function () {
//    seed(RoleSeeder::class);
//    /** @var IntegrationTestCase $this */
//    $this->mock(SendMessageAction::class)->shouldReceive('execute');
//
//    $client = Client::factory()->asPasswordClient()->create();
//
//    $password = 'Test1234!';
//    $user = User::factory()->create(['password' => $password]);
//
//    $requestBody = [
//        "grant_type" => GrantTypeEnum::PASSWORD,
//        "client_id" => $client['id'],
//        "client_secret" => $client['secret'],
//        "scope" => [],
//        "username" => $user->login,
//        "password" => $password,
//    ];
//
//    postJson('/api/v1/oauth/token', $requestBody)
//        ->assertStatus(200);
//
//    assertDatabaseHas('oauth_access_tokens', [
//        'user_id' => $user['id'],
//        'client_id' => $client['id'],
//    ]);
//});
//
//test('POST /api/v1/oauth/token incorrect credentials', function () {
//    seed(RoleSeeder::class);
//    /** @var IntegrationTestCase $this */
//    $this->mock(SendMessageAction::class)->shouldReceive('execute');
//
//    $client = Client::factory()->asPasswordClient()->create();
//
//    $correctPassword = 'Test1234!';
//    $wrongPassword = 'Test1234!*';
//    $user = User::factory()->create(['password' => $correctPassword]);
//
//    $requestBody = [
//        "grant_type" => GrantTypeEnum::PASSWORD,
//        "client_id" => $client['id'],
//        "client_secret" => $client['secret'],
//        "scope" => [],
//        "username" => $user->login,
//        "password" => $wrongPassword,
//    ];
//
//    logger()->setDefaultDriver('none');
//    postJson('/api/v1/oauth/token', $requestBody)
//        ->assertStatus(500)
//        ->assertJsonPath('errors.0.message', 'The user credentials were incorrect.');
//    logger()->setDefaultDriver(env('LOG_CHANNEL', 'stack'));
//});
//
//test('DELETE /api/v1/oauth/tokens/{id} success', function () {
//    seed(RoleSeeder::class);
//    /** @var IntegrationTestCase $this */
//    $this->mock(SendMessageAction::class)->shouldReceive('execute');
//
//    $client = Client::factory()->asPasswordClient()->create();
//
//    $password = 'Test1234!';
//    $user = User::factory()->create(['password' => $password]);
//
//    $requestBody = [
//        "grant_type" => GrantTypeEnum::PASSWORD,
//        "client_id" => $client['id'],
//        "client_secret" => $client['secret'],
//        "scope" => [],
//        "username" => $user->login,
//        "password" => $password,
//    ];
//
//    $testResponse = postJson('/api/v1/oauth/token', $requestBody);
//
//    $token = json_decode($testResponse->baseResponse->content())->access_token;
//    $tokenId = (new Parser(new JoseEncoder()))->parse($token)->claims()->all()['jti'];
//
//    deleteJson(
//        "/api/v1/oauth/tokens/$tokenId",
//        [],
//        ['Authorization' => 'Bearer ' . $token]
//    )
//        ->assertStatus(204);
//
//    assertDatabaseHas(
//        'oauth_access_tokens',
//        [
//            'id' => $tokenId,
//            'revoked' => true,
//        ]
//    );
//});
//
//test('DELETE /api/v1/oauth/tokens/{id} 404', function () {
//    seed(RoleSeeder::class);
//    /** @var IntegrationTestCase $this */
//    $this->mock(SendMessageAction::class)->shouldReceive('execute');
//
//    $client = Client::factory()->asPasswordClient()->create();
//
//    $password = 'Test1234!';
//    $user = User::factory()->create(['password' => $password]);
//
//    $requestBody = [
//        "grant_type" => GrantTypeEnum::PASSWORD,
//        "client_id" => $client['id'],
//        "client_secret" => $client['secret'],
//        "scope" => [],
//        "username" => $user->login,
//        "password" => $password,
//    ];
//
//    $testResponse = postJson('/api/v1/oauth/token', $requestBody);
//
//    $token = json_decode($testResponse->baseResponse->content())->access_token;
//    $tokenId = '123';
//
//    deleteJson(
//        "/api/v1/oauth/tokens/$tokenId",
//        [],
//        ['Authorization' => 'Bearer ' . $token]
//    )
//        ->assertStatus(404);
//});
