<?php

namespace App\Http\ApiV1\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateUserRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'login' => ['required', 'unique:users'],
            'active' => ['boolean'],
            'password' => ['nullable'],
            'first_name' => ['nullable'],
            'last_name' => ['nullable'],
            'middle_name' => ['nullable'],
            'timezone' => ['required', 'timezone'],
            'phone' => ['required', 'regex:/^\+7\d{10}$/', 'unique:users'],
            'email' => ['required', 'email', 'unique:users'],
        ];
    }
}
