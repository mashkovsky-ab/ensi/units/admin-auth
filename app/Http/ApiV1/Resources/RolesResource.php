<?php

namespace App\Http\ApiV1\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin \App\Domain\Users\Models\Role */
class RolesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'expires' => $this->whenPivotLoaded('role_user', function () {
                return $this->pivot->expires;
            }),
        ];
    }
}
