<?php

namespace App\Http\ApiV1\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin \App\Domain\Users\Models\User */
class UsersResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'login' => $this->login,
            'active' => $this->active,
            'last_name' => $this->last_name,
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'full_name' => $this->full_name,
            'short_name' => $this->short_name,
            'email' =>  $this->email,
            'phone' => $this->phone,
            'timezone' => $this->timezone,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'roles' => RolesResource::collection($this->whenLoaded('roles')),
        ];
    }
}
