<?php

namespace App\Http\ApiV1\Controllers;

use App\Domain\Users\Actions\CreateUserAction;
use App\Domain\Users\Actions\DeleteUserAction;
use App\Domain\Users\Actions\MassChangeActiveAction;
use App\Domain\Users\Actions\PatchUserAction;
use App\Domain\Users\Actions\RefreshPasswordTokenAction;
use App\Domain\Users\Models\User;
use App\Http\ApiV1\Queries\UsersQuery;
use App\Http\ApiV1\Requests\CreateUserRequest;
use App\Http\ApiV1\Requests\MassChangeActiveRequest;
use App\Http\ApiV1\Requests\PatchUserRequest;
use App\Http\ApiV1\Resources\UsersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Request;

class UsersController
{
    public function create(CreateUserRequest $request, CreateUserAction $action)
    {
        return new UsersResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchUserRequest $request, PatchUserAction $action)
    {
        return new UsersResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteUserAction $action)
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, UsersQuery $query)
    {
        return new UsersResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, UsersQuery $query)
    {
        return UsersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(UsersQuery $query)
    {
        return new UsersResource($query->firstOrFail());
    }

    public function current(Request $request)
    {
        /** @var User $user */
        $user = $request->user();
        if ($user) {
            $user->load('roles');
        }

        return $user ? new UsersResource($user) : new EmptyResource();
    }

    public function refreshPasswordToken(int $usedId, RefreshPasswordTokenAction $action)
    {
        $action->execute($usedId);

        return new EmptyResource();
    }

    public function massChangeActive(MassChangeActiveRequest $request, MassChangeActiveAction $action)
    {
        $action->execute($request->validated());

        return new EmptyResource();
    }
}
