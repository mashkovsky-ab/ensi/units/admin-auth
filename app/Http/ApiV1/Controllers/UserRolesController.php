<?php

namespace App\Http\ApiV1\Controllers;

use App\Domain\Users\Actions\AddRolesToUserAction;
use App\Domain\Users\Actions\DeleteRoleFromUserAction;
use App\Http\ApiV1\Requests\AddRolesToUserRequest;
use App\Http\ApiV1\Requests\DeleteRoleFromUserRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class UserRolesController
{
    public function add(int $userId, AddRolesToUserRequest $request, AddRolesToUserAction $action)
    {
        $validated = $request->validated();
        $action->execute($userId, $validated['roles'], $validated['expires'] ?? null);

        return new EmptyResource();
    }

    public function delete(int $userId, DeleteRoleFromUserRequest $request, DeleteRoleFromUserAction $action)
    {
        $validated = $request->validated();
        $action->execute($userId, $validated['role_id']);

        return new EmptyResource();
    }
}
