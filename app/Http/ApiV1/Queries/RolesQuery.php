<?php

namespace App\Http\ApiV1\Queries;

use App\Domain\Users\Models\Role;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class RolesQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = Role::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id', 'created_at', 'updated_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('title'),
        ]);

        $this->defaultSort('id');
    }
}
