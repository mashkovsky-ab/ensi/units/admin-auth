<?php

namespace App\Http\ApiV1\Queries;

use App\Domain\Users\Models\User;
use App\Http\ApiV1\Filters\FiltersUserFullName;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class UsersQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = User::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id', 'email', 'phone', 'active', 'login', 'created_at', 'updated_at']);
        $this->allowedIncludes(['roles']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('login'),
            AllowedFilter::exact('active'),
            AllowedFilter::exact('phone'),
            AllowedFilter::exact('email'),
            AllowedFilter::exact('role', 'roles.id'),
            AllowedFilter::custom('full_name', new FiltersUserFullName()),
            AllowedFilter::exact('password_token'),
        ]);

        $this->defaultSort('id');
    }
}
