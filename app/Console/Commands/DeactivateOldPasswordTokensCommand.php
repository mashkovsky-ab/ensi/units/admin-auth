<?php

namespace App\Console\Commands;

use App\Domain\Users\Actions\DeactivatePasswordTokenAction;
use Illuminate\Console\Command;

class DeactivateOldPasswordTokensCommand extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'passwords:deactivate-token';

    /**
     * The console command description.
     */
    protected $description = 'Деактивация токенов паролей, срок жизни которых истек';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(DeactivatePasswordTokenAction $action)
    {
        $action->execute();
    }
}
