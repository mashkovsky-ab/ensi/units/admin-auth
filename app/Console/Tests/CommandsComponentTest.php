<?php

use App\Domain\Kafka\Actions\Send\SendMessageAction;
use App\Domain\Users\Models\User;
use Database\Seeders\RoleSeeder;
use function Pest\Laravel\artisan;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\seed;
use Tests\ComponentTestCase;
use Tests\IntegrationTestCase;

uses(ComponentTestCase::class);
uses()->group('component');

test("Command passwords:deactivate-token success", function () {
    seed(RoleSeeder::class);
    /** @var IntegrationTestCase $this */
    $this->mock(SendMessageAction::class)->shouldReceive('execute');

    /** @var User $user */
    $user = User::factory()->create();
    $user->password_token_created_at = now()->subDays(User::LIFE_TIME_PASSWORD_TOKEN + 1);
    $user->save();

    artisan("passwords:deactivate-token");

    assertDatabaseHas((new User())->getTable(), [
        'id' => $user->id,
        'password_token' => null,
        'password_token_created_at' => null,
    ]);
});
